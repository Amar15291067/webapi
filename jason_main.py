
import json
from fastapi import FastAPI
from pydantic import BaseModel
import os
import hashlib

from sqlalchemy import true
""" to work on crud operations through the fast api 
and to open a json file to append the all the data into the the json file
create an empty list to append the all the data to the list
genarate a class post 
and to work on on_event(startup and shutdown)
"""
app = FastAPI()


posts=[]
users=[]

data={"posts":posts,"users":users}

class Post(BaseModel):
    username:str
    id:int
    branch:str

class User(BaseModel):
    username:str
    password:str
    email:str | None
class Users():
    def __init__(self,username:str =None,email:str=None, salt:str=None , hashed_password:str=None):# the none will create an empty object
        self.username=username
        self.email=email
        self.salt=salt
        self.hashed_password=hashed_password

    def hash_password(self, password: str):
        """ genarate the hashed password and store it"""
        self.salt = os.urandom(64).hex()#genarate a random string
        self.hash_algorithm = "pbkdf2-hmac-sha512"
        self.no_of_iterations = 100000# no.of iterations
        # converted to bytes to and then converted to hexa value with hex.()
        self.hashed_password = hashlib.pbkdf2_hmac("sha512", password.encode("utf-8"), self.salt.encode("utf-8"), self.no_of_iterations).hex()

    def dict_user(self):
        return{
            "username":self.username,
            "email":self.email,
            "hashed_password":self.hashed_password,
            "salt":self.salt
        }
    def from_dict(self,dict_obj):
        self.username=dict_obj["usernane"]# if we dont asign a key to the dict it will look for the key and return the error as there is no key
        self.email=dict_obj["email"]#by asigning thise thise will look for the key and return the value 
        self.salt=dict_obj["salt"]
        self.hashed_password=dict_obj["hashed_password"]

    def to_varify(self,password:str):
        new_hash=hashlib.pbkdf2_hmac("sha512", password.encode("utf-8"), self.salt.encode("utf-8"),100000)
        if new_hash.hex()==self.hashed_password:
            return True
            
        else:
            return False

@classmethod
def load_from_dict(cls,dict_obj):
    return users (
        username=dict_obj["username"],
        email=dict_obj["email"],
        salt=dict_obj["salt"],
        hashed_password=dict_obj["hashed_password"]
    )

@app.post("/signup")
def sign_up(user:User):
    for entry in users:
        return{
            "message":"already existed"
        }
    new_user=users(username=user.username, email=user.email)
    new_user.hash_password(user.password)
    users.append(new_user.dict())
    return{
        "message":"user added successfully"
    }

@app.post("/login")
def login (user:User):
    for entry in users:
        if entry ["username"]==user.username:
            check_user=users.load_from_dict[entry]
            check_user.varify(user.password)
            return{
                "message":"login authrnticated"
            }
        else:
            return{
                "message":"worng password"
            }
    return{
        "message":"username not found in our data base"
    }
                   

@app.post("/post/add")#to add the elements
def post_item(post:Post):# create a normal function 
    data.append(post.dict())# the data will be in the dict mode

    return {
        "message": "data added succesfully",
        "user_added":{#raise a msg to the user what he added
            "username":post.username,
            "id":post.id,
            "branch":post.branch
        }
    }


@app.get("/posts")# to get the data what user added
def get_users():
    return{
        "results":data
    }

@app.post("/users")# for checking the user respect to our data base
def check_users(user:User):
    if user in data:
        return " the user is already in the data base"
    else:
        return " you can add the data to our data base"

# @app.post("/sign_up")
# def user_signup(user:User):
#     if user in data:
#         return "already thise user name is exixted in database"
#     else:
#         data.append(user.dict())
#         return {
#             "message":"data added succesflly"
#         }

@app.delete("/posts/delete/{branch_name}")# to delete the post ({branch_name} we can pass the deleting parameter with a name)
def delete_post(branch_name:str):# make sure it is a what kind of data type
    for entry in data:#generate a for loop for deleting the data 
        if entry["branch"]==branch_name:
            data.remove(entry)
            return {#raise a meassage to the user what is deleted
                "message":"post removed successfully",
                "post":entry
            }
    return {
        "message":"post not found"
    }

@app.on_event("startup")
def data_load():
    loading_file="data_add.json"#create a json file to store the data with a name to the file
    if os.path.exists(loading_file):# check the file with os.path.exists
        with open ("data_add.json","r")as file:
            global data,users,posts
            data=json.load(file)# now load the file into json format           
            posts=data["posts"]
            users=data["users"]

@app.on_event("shutdown")
def save_file():
    with open("data_add.json","w")as file:
        json.dump(data,file)

