# -*- coding: utf-8 -*-
"""
Created on Mon Feb 28 10:17:57 2022

@author: anagella
"""

from fastapi import FastAPI
from haspass import DBSession, Items, Users
from sqlalchemy import select
from pydantic import BaseModel

app = FastAPI()


class Item(BaseModel):
    item_name: str
    item_description: str
    item_image_url: str

class Users(BaseModel):
    user_name:str
    user_password:str
    user_email:str


@app.get("/item/{item_id}")
async def get_item(item_id: int, item_name: str | None = None):
    async with DBSession() as session:
        async with session.begin():
            sql_stmt = select(Item).where(Item.id == item_id)
            print(sql_stmt)
            results = await session.stream_scalars(sql_stmt)
            result: Item = await results.first() #
    return {
        "item_name": result.item_name,
        "item_description": result.item_description,
        "item_image_url": result.item_image_url
    }


@app.get("/Users/{Users_id}")
async def get_Users(Users_id: int, users_name: str | None = None):
    async with DBSession() as session:
        async with session.begin():
            sql_stmt = select(Users).where(Users.id == Users_id)
            print(sql_stmt)
            result = await session.stream_scalars(sql_stmt)
            result: Users = await result.first()
    return {
        "username": result.username,
        "hash_algorithm": result.hash_algorithm,
        "no_of_iterations": result.no_of_iterations,
        "salt": result.salt,
        "hashed_password": result.hashed_password,
        "email": result.email
    }


@app.post("/item/add")
async def add_item(item: Item):
    async with DBSession() as session:
        async with session.begin():
            try:
                item_to_add = Items(**item.dict())# item will converted into dict,check the__dict__function once or create a dict function in models_data
                session.add(item_to_add)
                await session.commit()
                return {
                    "message": "item added",
                    "added_item": item.dict()
                }
            except ValueError as err:
                return {
                    "message": str(err),
                }


@app.post("/Users/add")
async def add_item(user: Users):
    async with DBSession() as session:
        async with session.begin():
            try:
                user_to_add =Users(user_name=user.user_name)
                user_to_pass=Users(user_password=user.user_password)
                user_mail=user(user_mail=user.user_email)
                session.add(user_to_add)
                session.add(user_to_pass)
                session.add(user_mail)
                await session.commit()
                return {
                    "message": "users added",
                    "added_item": user.dict()
                }
            except ValueError as err:
                return {
                    "message": str(err),
                }
