from curses import meta
from sqlalchemy.orm import Session
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.future import Engine
from sqlalchemy import Column,String,Integer,Float,MetaData
import requests
import json


engine: Engine= create_engine("sqlite:///main.db")
Base=declarative_base()
session=Session()
meta=MetaData

class Responses(Base):
    __tablename__="coins_price_table"
    row_no : int = Column(Integer,primary_key=True)
    id : str = Column(String(64),unique=True)
    localization : str = Column(String(64))
    curent_value : float = Column(Float)
    price_change: float = Column(Float)
def price_change_(self,time):
        resp=requests.get("https://api.coingecko.com//api/v3/coins",params={"id":self.id,
        "localization":self.localization,
        "current_value":self.current_value,
        "price_change":self.price_change})
        
meta.create_all(engine)