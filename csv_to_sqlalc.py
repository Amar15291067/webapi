from email.mime import base
from sqlalchemy import Column, Integer, Float,create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker,Session
import pandas as pd
from sqlalchemy.future import Engine


engine:Engine=create_engine("sqlite+pysqlite:///rain_fall.db")
session=Session(engine)
Base=declarative_base()


with open ("C:\\Users\\anagella\\Desktop\\rainfall in india 1901-2015.csv", "r") as file:
    rain_data=pd.read_csv(file)
rain_data.to_sql("rainfall in india",con=engine,index_label="id")

Base.metadata.drop_all(engine)
Base.metadata.create_all(engine)



